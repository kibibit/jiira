const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

const getAll = async (req, res) => {
  try {
    const companies = await prisma.company.findMany({})
    if (companies.length === 0) return res.status(204).json()
    res.status(200).json(companies)
  } catch (eer) {
    return res.status(500).json({ message: eer.message })
  }
}

module.exports = { getAll }
