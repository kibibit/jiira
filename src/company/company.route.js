const express = require('express')
const router = express.Router()
const Company = require('./company.controller')

router.route('/').get(Company.getAll)

module.exports = router
