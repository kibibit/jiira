const jwt = require('jsonwebtoken')

const auth = async (req, res, next) => {
  try {
    const token = req.header('Authorization').replace('Bearer ', '')
    const user = await jwt.verify(token, process.env.JWT_SECRET)
    req.user = {
      userId: Number(user.userId),
      email: user.email,
      companyId: Number(user.companyId)
    }
    next()
  } catch (error) {
    res.status(401).send({ error: 'Not authorized to access this resource' })
  }
}

module.exports = auth
