const express = require('express')
const routes = express.Router()

const userRouter = require('./user/user.route')
const companyRouter = require('./company/company.route')

routes.use('/user', userRouter)
routes.use('/company', companyRouter)

module.exports = routes
