const express = require('express')
const app = express()
const cors = require('cors')

const port = process.env.PORT || 3000

app.use(cors())
app.use(express.json())

const apiRouter = require('./routes')
app.use(apiRouter)

app.listen(port, () => console.log(`server running on port ${port}`))
