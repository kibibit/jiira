const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

async function main() {
  console.log('starting push data...')
  const mebi = await prisma.company.upsert({
    where: { id: 1 },
    create: {
      email: 'info@mebi.com',
      name: 'Mebi',
      nit: '9059059058',
      phone: '3003134455',
      address: 'calle 45 13 17',
      users: {
        create: [
          {
            name: 'Julio Cortes',
            email: 'julio.cortes@mebi.com',
            password: '$argon2i$v=19$m=4096,t=3,p=1$IfBVuF73MYzdcJvQukhlTA$Ghjhj9GJrA1+cQ62otWcLtTRlzMICR75tngHG2U14JQ'
          },
          {
            name: 'Carlos Garzon',
            email: 'carlos.garzon@mebi.com',
            password: '$argon2i$v=19$m=4096,t=3,p=1$dMo/UVtrjVk/EbDFx20Lqg$/1r6Qi2S4O8yG4BLsAaV7yvRBA5j7uxIvAQ7EhE7180'
          }
        ]
      },
      projects: {
        create: [
          {
            name: 'Chat para VideoJuego',
            slug: 'CV'
          },
          {
            name: 'Asteroids',
            slug: 'AS'
          }
        ]
      }
    },
    update: {}
  })

  const kibi = await prisma.company.upsert({
    where: { id: 2 },
    create: {
      email: 'info@kibi.com',
      name: 'Kibi',
      nit: '9069069067',
      phone: '3156783214',
      address: 'calle 45 13 19',
      users: {
        create: [
          {
            name: 'Jose Mendez',
            email: 'jose.mendez@kibi.com',
            password: '$argon2i$v=19$m=4096,t=3,p=1$j3AtKN3CMQBCYdUTzvjXQA$/nWvTVsrw2yCwl8Bt5QjHTXKUH1zYLdk1mTO4XtyNcg'
          },
          {
            name: 'Diego Cadavid',
            email: 'diego.cadavid@kibi.com',
            password: '$argon2i$v=19$m=4096,t=3,p=1$Kg9Qtho9XugtzmxX1t/aCg$AsDHall5o/7HmdfjO2Jw/OBN5xOOdWAAtBb5XcjGB6Y'
          },
          {
            name: 'Sebastian Camargo',
            email: 'sebastian.camargo@kibi.com',
            password: '$argon2i$v=19$m=4096,t=3,p=1$4j7Lv/8Vx30TSxz76yfhHg$eQ394Gc6OTsTMMiRRC99yeTDV6RMsgiN/lvcUbB+Rjk'
          }
        ]
      },
      projects: {
        create: [
          {
            name: 'CRM Supermercados',
            slug: 'CS'
          },
          {
            name: 'ERP Abogados',
            slug: 'EA'
          },
          {
            name: 'POS ferreteria',
            slug: 'PF'
          }
        ]
      }
    },
    update: {}
  })
  console.log({ kibi, mebi })
}

main()
  .catch(e => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
